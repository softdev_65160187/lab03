/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com_mycompany.lab3;

/**
 *
 * @author informatics
 */
class ox {
    static String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
    
    static boolean checkWin(String[][] table, String currentPlayer) {
        
        if(checkRow(table,currentPlayer)){
            return  true;
        }else if(checkCol1(table,currentPlayer)){
            return  true;
        }else if(checkCol2(table,currentPlayer)){
            return  true;
        }return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer) {
        for(int row=0;row<3;row++){
            if(checkRow(table,currentPlayer,row)){
            return  true;
            }
        }return false;
    }
    
    private static boolean checkRow(String[][] table, String currentPlayer,int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean checkCol1(String[][] table, String currentPlayer) {
        return table[0][0].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][2].equals(currentPlayer);
    }
    
    private static boolean checkCol2(String[][] table, String currentPlayer) {
        return table[0][2].equals(currentPlayer) && table[1][1].equals(currentPlayer) && table[2][0].equals(currentPlayer);
    }
    
    public static boolean checkDraw(String[][] table) {
       return table[0][0].equals("-") && table[0][1].equals("-") && table[0][2].equals("-") && table[1][0].equals("-") && table[1][1].equals("-") && table[1][2].equals("-") && table[2][0].equals("-") && table[2][1].equals("-") && table[2][2].equals("-");
    }

    public static String[][] getTable() {
        return table;
    }

}
